const btnLoan = document.getElementById("loan")
const btnWork = document.getElementById("work")
const btnBank = document.getElementById("bank")
const btnBuyComp =document.getElementById("buyComp")
const computersElement = document.getElementById("computer")
const descriptionElement = document.getElementById("description")
const compNameElement = document.getElementById("computerName")
const compDiscElement =document.getElementById("computerDescription")
const priceElement =document.getElementById("price")

//The base url for finding the images
const baseUrl = "https://noroff-komputer-store-api.herokuapp.com/"

let btnPayLoan = document.createElement("button")
btnPayLoan.setAttribute("id", "payBnt")

let pay = document.getElementById("payment")
let bankValue = document.getElementById("val")
let loanTxt = document.getElementById("loanText")
let loanVal = document.getElementById("loanSum")

//Keep track of the current amount in the bank and the current amount of loan
let loan = 0;
let moneyInBank = 0;

//Creating event for all buttons in website
btnLoan.addEventListener("click",buttonForLoan)
btnWork.addEventListener("click",buttonForWork)
btnBank.addEventListener("click",buttonForBank)
btnBuyComp.addEventListener("click", buyAComputer)


function buttonForLoan() {
    //When button for loan is pressed, a prompt appears to ask for amount
    let amount = parseInt(prompt("Please enter wanted loan amount"))
    //If you already have a loan, an alert is given that you can not take another
    if (loan != 0) {
        window.alert("You already have a loan. Can not create another")
        return
    } 
    //If no amount is given or cancel is pressed we just exit the function
    if (amount == null || amount == "") {
        return
    }
    /*If the loan amount is within accepted range, the loan value is increased.
    //This makes the loan text and value,and the pay loan button appear.
    //Money in bank increases with the loan
    //If loan amount is too high, a prompt is given to try another amount or cancel
    */
    if (amount <= moneyInBank*2) {
        loan = amount
        loanTxt.innerText = "Total loan";
        loanVal.innerText = loan + " kr";
        moneyInBank += amount;
        bankValue.innerText = moneyInBank + " kr"
        createPayLoanButton()
    }
    else {
        prompt("Loan is too high for you. Try with other amount or cancel loan.")
    }  
}

function createPayLoanButton() {
    //Button appears when a loan is made
    btnPayLoan.innerHTML = "Pay loan"
    btnPayLoan.addEventListener("click", buttonPayLoan)
    btnPayLoan.className = "buttonPayLoan"
    document.getElementById("payLoan").appendChild(btnPayLoan)
    
}

function removePayLoanAttributes() {
    //Button is removed when loan is payed
    loanTxt.innerText = "";
    loanVal.innerText = "";
    let elem = document.getElementById("payBnt")
    elem.remove()
}

function buttonPayLoan() {
    //Amount is subtracted from loan. If amount is higher then loan, the rest is added to bank
    value = parseInt(pay.innerText.split(" ")[0])
    loan -= value;

    if (loan <= 0) {
        moneyInBank -= loan
        loan = 0;
        bankValue.innerText = moneyInBank + " kr"
        loanVal.innerText = loan + " kr"
        pay.innerText = 0 +" kr"
        
        removePayLoanAttributes()
    }
}

function buttonForWork() { 
    //Increases pay with 100 for each click
    value = parseInt(pay.innerText.split(" ")[0])
    pay.innerText = (value + 100) + " kr"
}

function buttonForBank() {
    //Press button to put payment into the bank
    value = parseInt(pay.innerText.split(" ")[0])
    bank = parseInt(bankValue.innerText.split(" ")[0])
    //If there is no loan, all payment goes to the bank account
    if (loan == 0) {
        moneyInBank = value + bank
        bankValue.innerText = moneyInBank + " kr"
    }
    //If there is a loan, 10% is used to down-pay loan. The rest goes to the bank account
    //If the down-payment is greater then the loan, the rest is added to the bank account, else the new loan value is set
    else {
        deduct = value*0.1
        loan -= deduct
        if (loan < 0) {
            deduct += loan
            loan = 0;
            removePayLoanAttributes()
        }
        else {
            loanVal.innerText = loan + " kr"
        }

        moneyInBank = bank + (value-deduct)
        bankValue.innerText = moneyInBank + " kr"

    }
    pay.innerText = "0 kr"
}


let computers = []
let features = []


fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToStore(computers));


function addComputersToStore(computers) {
    //add all computers to the select bar
    computers.forEach(x => addComputerToStore(x))

    //Create the values for the first computer
    firstComp = computers[0].specs

    items = ""
    firstComp.forEach(x => items += x + "\n")

    descriptionElement.innerText = items
    compNameElement.innerText = computers[0].title
    compDiscElement.innerText = computers[0].description
    priceElement.innerText = computers[0].price + " kr"
    url = baseUrl + computers[0].image
    const block = document.getElementById("img")
    block.setAttribute("src", url)
}

function addComputerToStore(computer) {
    //Add one computer to the select bar
    const computerElement = document.createElement("option")
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}

function handleComputerChange(event) {
    //Handling changes in the select bar
    const selectedComputer = computers[event.target.selectedIndex]
    items = ""
    comp = selectedComputer.specs.forEach(x => items += x + "\n")
    descriptionElement.innerText = items

    items = ""
    firstComp.forEach(x => items += x + "\n")

    compNameElement.innerText = selectedComputer.title
    compDiscElement.innerText = selectedComputer.description
    priceElement.innerText = selectedComputer.price + " kr"
    
    img.src = baseUrl + selectedComputer.image
    let block = document.getElementById("img")
}

computersElement.addEventListener("change", handleComputerChange)

function buyAComputer() {
    //If bank account has enough money, a click on buy computer will alert you that computer is bought, else
    //you are informed that you do not have enough money.
    price = parseInt(priceElement.innerText.split(" "))
    if (price <= moneyInBank) {
        moneyInBank -= price
        bankValue.innerText = moneyInBank + " kr"
        alert("You just bought a new computer!")
    }
    else {
        alert("You do not have enough money. Work harder")
    }
    
}
